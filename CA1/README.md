# DEVOPS CA1 TECHNICAL REPORT #

## Version Control: Git
The goal of the assignment is to use version control with **Git** and learn how to work with it.  

On the first part of the assignment, work was developed without using branches and everything on the master branch.  
The second part requires the uses of branches and publishing a stable version on the master branch.

It is also required to analyze and implement the solution using an alternative
version control system.

## Requirements
### Analysis
The task at hand requires the tracking of changes done to a project. These changes include
adding files to a directory, modifying those files and creating new ones. The project should also keep track of the
latest application version.

In order to keep track of the changes, Git was used as version control.  
Git is a distributed version control system that allows for tracking changes in any set of files.
Every Git directory on every computer is a repository without the need for a central server.  
Remote repositories are commonly used so that the development of source code between programmers
can be coordinated effectively.

The following implementation will demonstrate how to complete the task, recording changes to a project
developed locally and pushing the changes to a remote repository.

### Implementation
In order to implement the task, the Git Command Line will be used to perform Git commands.  
The remote repository will be available at Bitbucket.

To start using Git, follow the next steps.

*(All commands should be typed in a command line)*

#### 1. Initializing the repository
A remote repository must be created directly on Bitbucket.
In order to work locally with this repository a copy must be acquired from the remote. 
To get a copy from a remote repository, in the command line change to the directory where the copy
will be stored and type the following command:  
`git clone https://angelmagalhaes_1211750@bitbucket.org/angelmagalhaes_1211750/devops-21-22-lmn-1211750.git`  
The clone command implicitly adds the origin remote repository.
  
Alternately, a repository can be initialized locally in a directory that is not under version control by
changing to the directory and type the following command:  
`git init`  

The remote should be added explicitly in this case:  
`git remote add origin https://angelmagalhaes_1211750@bitbucket.org/angelmagalhaes_1211750/devops-21-22-lmn-1211750.git`

Regardless of the approach, the project directory has been initialized to be tracked by Git,
and changes done to the source code will be tracked.

#### 2. Adding new files to the project
On the local directory, copy the code from the **Tutorial React.js and Spring Data REST Application** into
a new folder name **CA1**.  

To check if the files were added, type the following command:  
`git status`  

Check that the files were added and are currently *untracked*. This means that any changes made to these files will not be
considered. Git doesn't track new files unless instructed to do it. The files can be added one by one or all at once.
To add all the new files, type the following command:  
`git add .` This command takes either a file or a directory (in this case the 'dot' means to add the current directory,
which should be the root of the project).
By typing `git status` again, it is possible to check that the files are now in the 'staging area' and can be added to
the 'snapshot'

The purpose of these 'snapshots' is to capture the state of the project at that point in time.  
To do this, a 'commit' must be created using the following command:  
`git commit -m "Add files to CA1 folder"`  

It is possible to just enter `git commit` but since a message is required, the `-m` option allows to write a commit message 
directly in the command without the need to open a text editor. 
In the event that something is missing from the previous commit or there is a typo in the message, the command `git commit --amend`
can be used to change the previous commit without the need to make a new one.

After the commit, the working directory should be without files (check with `git status`).  
The log of commits can be checked with `git log`. The latest should be recorded there.

Next step is to update the remote repository with the local changes. In order to do that, type the following command:  
`git push origin master`. The terms 'origin' stands for the remote's name and 'master' stands for the branch name.

Finally, the application should be marked for the current version. In order to do that, type the following command:  
`git tag -a v1.1.0 -m "initial version"`.  

This will tag the project's current state with the 'v1.1.0' tag. The `-a` option creates an annotated tag, which stores 
extra metadata, along with a message (using the `-m` option to avoid prompting the text editor).  
Since tags are not transferred by default to the remote repositories, it has to be explicitly pushed in order to do so:  
`git push origin v1.1.0`. This will 'push' the tag to the 'origin' remote.

#### 3. Making changes in the project
A new feature has been requested for the application. In order to implement it, the application files will be modified.  
Changes were made to the 'Employee.cs' file to add support for a new field. Changes to an existing file must be stage
so a commit can be made. This can be done with the 'add' command previously used:  
`git add Employee.cs` (if not in file the folder, the path to the file should also be specified)  
`git commit -m "add new field"`

Several commits can be made before pushing to the remote server.  
Unit tests for the creation of Employees was also requested with this new feature, so test files were added and several 
files were modified. After completion and debugging the solution, a commit can be made:  
`git add .` (if in the root directory of the project) or `git add -A` to stage all the new and/or modified files.  
`git commit -m "add unit testing"`

Finally, the changes can be pushed to the remote server and a tag can be added to signal the latest version of the application.  
`git push origin master`  
`git tag -a v1.2.0 -m "version 1.2.0 with more fields"`  
`git push origin v1.2.0`

To signal that the first part of the assignment is finished, another tag is added:  
`git tag -a ca1-part1 -m "part 1 of CA1"`

#### 4. Working with branches
A new feature was requested for the application. It is possible to develop this feature in a separate environment from the 
stable version of the application using branches. When a commit is made, a 'snapshot' of the changes are saved to an object that contains 
a pointer to that snapshot.  
A branch is a lightweight pointer to those commits. Git keeps track of the branching being worked by using a special pointer
called HEAD. To create a new branch, type the following command:  
`git branch email-field`  

This will create a new branch called *email-field* and will contain all the information from the branch it was created from.
In this case, the branch was created from the *master* branch.  
A list of existing branches can be view by typing the following command: `git branch`.  
The branched marked with the asterisk (*) represents the current branch being worked, in this case, it will be the master branch.

To start working in the new created branch, the pointer HEAD must be moved to it. Currently, HEAD is pointing to the master branch,
so it must be changed to the new branch with the following command:  
`git checkout email-field`

HEAD is now pointing to the *email-field* branch so all the changes and commits done in this branch will not affect the master
branch.  
After implementing the feature, commit the changes in this branch:  
`git add .`  
`git commit -m "adds new feature"`

Now the changes are only present in the *email-field* branch. When changing back to the *master* branch, the local files will
revert to the state of the latest commit on that branch. The changes done in the new branch must be merged with the master branch.  
To do that, the current working branch must be changed to the master or preceding branch:
`git checkout master`

After changing to the branch, the changes can be merged by typing the following command:
`git merge email-field`  

The state of the branches and the last commit can be checked with `git branch -v`. This can help to check if both branches
have the same commit, which indicates that the merge was successful.
The latest changes are now ready to be pushed to the remote repository:  
`git push origin master`  
`git tag v1.3.0`  
`git push origin v1.3.1`

Since the branch has filled his purpose, there is no need for it to exist anymore, so it can be safely deleted:  
`git branch -d email-field`.
This command will fail if there are unmerged changes.

Now, a fix to the new feature has been requested, so a new branch must be created to handle the changes. The following command
will create the branch and at the same time checkout to it:  
`git checkout -b fix-invalid-email`. By default, this branch will be based wherever the HEAD is pointing to.

Since the current working branch is the *fix-invalid-email* branch, work can be developed to fix the bug. After fixing the bug,
commit the changes and merge it back to master branch.  
`git add .`  
`git commit -m "fixes email field bug"`  
`git checkout master`  
`git merge fix-invalid-email`  
`git branch -d fix-invalid-email`

While the bug was being fixed, new changes were pushed to the remote repository by other team members, meaning that the current
local repository will not be in the same level. By typing the following command, changes made to the remote can be seen:  
`git fetch origin master`  
By checking with `git status`, the following message will appear:  
>Your branch and 'origin/master' have diverged,
and have 1 and 1 different commits each, respectively.

In order to fix this, a merge to the remote repository must be made:  
`git merge origin/master`  
This command will merge the differences in a new commit. The sequence of *fetch* and *merge* from a remote can be simplified 
by just typing the command `git pull`.

Since there are no divergences in the branch, the changes can then be pushed to the remote:  
`git push origin master`  
`git tag -a v1.3.1 -m "version 1.3.1 fixes email bug"`  
`git push origin v1.3.1`

#### 5. Resolving conflicts

When merging branches, sometimes a conflict can appear, that means that there were changes made to the same line(s) in a
file. Git cannot determine which of these changes it should keep. When this happens, Git will mark the file as being in conflict
and will not merge. The developers' should resolve this conflict.

By checking the file we can examine the marks applied to the file:
`cat README.md`  
The marks are the following:
* <<<<<<<
* =======
* \>>>>>>>

These are "conflict dividers", with the ======= being the "center" of the conflict.
All lines between the *center* and <<<<<<< is content that exists in the current branch where the HEAD is pointing,
while all content between the *center* and >>>>>>> is content that exists in the merging branch (currently the remote branch origin/main).

To solve this conflict the quickest way is to open the conflicted file and remove all the conflict dividers with the appropriate changes to the file.

After the changes, by checking with `git status` the following message can be seen:

>You have unmerged paths.
>  (fix conflicts and run "git commit")
>  (use "git merge --abort" to abort the merge)
>
>Unmerged paths:
>  (use "git add <file>..." to mark resolution)
>        both modified:   README.md

So add the file to stage `git add README.md` then finalize the merge with a new commit:  
`git commit -m "merged and resolved the conflict in README.md"`

Finally, push the changes to the remote repository `git push origin master`.

*****
## Alternative Solution: SVN

### Analysis
**Apache Subversion**, commonly abbreviated to **SVN**, is an open source version control system.  
It is a *centralized* version control system, meaning it has only one central repository and all the data is stored in that
server. All the developers work under the same repository.  
This allows the development in a more top-down approach to control and security, and it also becomes easier to implement new features. 

With SVN, all version-controlled project files are stored in a central database, *The Repository*. This is the definitive version that
supplies content when requested. All changes and historical data are stored in this server.  
Developers who work in the project must have a *Working Copy* in their local PC. This means that only the code to be changed
is copied. A developer can pull down the latest version from the repository, but it does not contain the history of the project.
When changes made to the working copy are deemed complete they are then merged back with the repository.   
A direct network connection to the repository is required to make all these operations work.


When compared to Git (distributed system), there are some key differences regarding development:

* Since every git user has their own copy that works as a "central" repository, any changes and operations don't require
a network connection to the remote server. In SVN, a connection is needed to run the operations.
* If there is an error in an SVN project it can be problematic since it affects all builds and no work can be done until it is
fixed. With Git, this is a non-issue since work is developed locally, so even if the central repository is broken it won't
slow down development.
* Storing large binary files in Git is not practical since every time a file is changed, there is a need to retrieve the full repository
to the local machine. In SVN only the working tree and the latest changes are checked to the local machine, taking less time
when there are a lot of changes in those files.
* SVN has an easier learning curve than Git, since it has a straightforward workflow, while learning Git CLI (command-line interface)
can be a bit overwhelming.

### Implementation
The alternative solution implementation repository is hosted at Sourceforge.  
**Commit history can be viewed by following this link:** [Alternative Solution Repository](https://sourceforge.net/p/devops-21-22-lmn-1211750/svn/commit_browser)

#### AS1. Initializing the repository
The repository was created and initialized in the remote server, but it is possible to initialize it locally with the following command:  
`svnadmin create Repository-Name`

After creating the repository, it is possible to commit tree changes to it without an explicit commit action. One of those commands
is the `svn import` command, which can commit an unversioned tree of files to the repository:

`cd path_to_files`  
`svn import https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/ -m "Initial commit"`

#### AS2. Adding files
As shown previously, it is possible to make changes to the repository tree directly, but work is usually done first on a working
copy in a local machine. To do that, a *Working Copy* must be obtained with the following command:

`svn checkout https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/`

When making changes to existing files, SVN detects it automatically. Tree changes (adding or removing files for example) 
must be specified. Before adding any files, it might be a good ideia to specify which files are not necessary to be added
to version control. In this implementation, node_module and target folder files are not required, so they can be safely ignored.
There are several ways to do this. They can be configured previously in the SVN config file (in windows it is located at
C:\Users\{you}\AppData\Roaming\Subversion\config) by adding the following line: `global-ignores = node_modules target`.
Another way is to simply not add them. 

The command to add files is the following:  
`svn add FILE_OR_FOLDER`. If it is a folder, everything underneath will be scheduled to be committed, so if we don't want to
include subfolder to be added, the `--depth files` option can be specified (this option will copy only the files underneath).
If only the folder is to be included, then the `--depth empty` option can be specified.

After adding files and/or making changes, it is a good ideia to check it review the changes using the `svn status` command.  
When confirming that the files are being tracked, they can be sent to the repository with the following command:  

`svn commit -m "Added files"`

Unlike Git, a *commit* in SVN will send the changes directly in the repository. 

#### AS3. Making changes in the project
Before making changes, when working in collaboration with other developers, it is possible that other working copies modified 
the repository, so it is generally a good workflow to update it via the following command:  

`svn update`

After the working copy has been updated and the changes have been made, it is possible to compare the changes with the previous
version of a file by using the following command:  

`svn diff`. This will display the changes in the *unified diff* format.

If while viewing the output of this command it is determined that the changes are a mistake, it is possible to revert the changes
easily using the following command:  

`svn revert MODIFIED_FILE`. This command also works when adding undesired files or recovering mistakenly deleted files, since it
reverts the status of the file to its previous state.

If the changes were accepted, the changes can then be sent to the repository:  
`svn commit -F logchanges`. This option allows to add a commit message from a file.

To finish this part of the assignment, the app version should be marked for release. SVN doesn't have special commands for tagging
since each repository revision is exactly a snapshot of the filesystem in a specific time. However, it is possible to make it
more human friendly, or taking snapshots of smaller subdirectories of the system. For that, the `svn copy` command can be used.  
First, a tag folder should be created directly in the repository:  
`svn mkdir -m "Creates tag folder" https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/tags`

Then the copy should be added to the tag folder:  
`svn copy https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/tags/ca1-part1 
-m "Tagging ca1-part1 version of the assignment"`

#### AS4. Working with branches
Like creating a tag, working with branches is similar. In order to do that, it's easier to follow a folder structure that separates
the main trunk, tags and branches. Since SVN doesn't have branching features like Git does, it works by using copies of the project
in a directory that has meaning to it.

After changing the folder structure, a new branch can be created by copying the files remotely:  
`svn copy https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/trunk https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/branches/email-field -m "Creating branch to add email field"`

Then, just checkout the branch or change the current working copy directory with the following command. On the current working
copy root, type the following command:  
`svn switch https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/branches/email-field .`

After making the changes they can be committed. Another team member also committed changes to the trunk. 
It is best to keep the branch in sync, therefore, a *sync merge* should be performed. To do that, type the following command:  
`svn merge https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/trunk`
After this, check that the working copy contains the new modification `svn status`. These changes must be committed:  
`svn commit -m "Merged latest trunk changes to branch"`

After checking that the branch is in sync with the trunk, a *reintegration* must be performed in order to keep the trunk
with the changes done in the branch. To do that, a working copy of the trunk is necessary. In this case, just switch back to it:  
`svn switch https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/trunk .`  
Then check that the trunk is up do date `svn update`.
Finally, type the `merge` command with the `--reintegrate` option:  

`svn merge --reintegrate https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/branches/email-field`
`svn commit -m "Merged the email-field branch back into trunk`

#### AS5. Resolving conflicts
When trying to merge or update the working copy, conflicts can occur when other team members made changes
to the file working on. As an example, when editing the README file and doing a `svn update`, a conflict occurred. The following
information was displayed, asking to choose an option to resolve it:  
>Summary of conflicts:
Text conflicts: 1
Merge conflict discovered in file 'README.md'.
Select: (p) Postpone, (df) Show diff, (e) Edit file, (m) Merge,
(s) Show all options:

When choosing (e) to edit, the editor specified in the EDITOR environment variable will open.
For every conflicted file, SVN places three extra unversioned files in the working copy:
* README.mine
* README.rOLDREV
* README.rNEWREV

This files will not allow SVN to commit the changes done, so the conflict must be resolved.
The best way to do this is by manually removing the markers in the file (like Git, SVN also places similar markings when
finding conflicts).

After removing the markers and leaving the desired changes, the following command can be type to resolve the conflict:  
`svn resolve --accept working README.md`
Now all that's left is to commit the changes:
`svn commit -m "Added another line to README.md`

After these changes, all that's left is to tag once again the latest version of the app.  
`svn copy https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn https://angelsanchez3@svn.code.sf.net/p/devops-21-22-lmn-1211750/svn/tags/ca1-part2 
-m "Tagging ca1-part2 version of the assignment"`

### Conclusion
After experimenting with both Git and SVN, my understanding of version control systems has widely grown, and I now consider
them to be a fundamental tool to use for every developer. From the two, I found out that Git is more useful and powerful to use
on a day-to-day basis and even while working with other developers.

From my experience, I found out that Git has a higher learning curve that SVN, but it becomes easier to work with over time. 
Having no branching feature in SVN and the workaround it being more time-consuming is one of the major drawbacks I personally 
found out while experimenting with SVN. On the other hand, most features of SVN are easy to use and understand, 
so I can see it as useful tool for keeping version control within a solo project, or even when working with smaller teams.