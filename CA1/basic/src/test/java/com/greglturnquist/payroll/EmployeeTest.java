package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    void employeeIsCreatedWithValidFields(){
        //Arrange + Act
        Employee employee = new Employee
                ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "gandalf@rivendell.com");

        assertNotNull(employee);
    }

    @Test
    void shouldNotCreateEmployeeWithInvalidTextFields(){
        //Arrange
        String expected = "Invalid Fields";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("", "The Grey", "Is a Maiar", "Wizard", 24000, "gandalf@rivendell.com"));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotCreateEmployeeWithInvalidJobYears(){
        //Arrange
        String expected = "Invalid job years";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("Gandalf", "The Grey", "Is a Maiar", "Wizard",
                        -30, "gandalf@rivendell.com"));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotCreateEmployeeWithEmptyEmail(){
        //Arrange
        String expected = "Invalid Fields";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, ""));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotSetFieldIfTextIsInvalid(){
        //Arrange
        String expected = "Invalid field";
        Employee employee = new Employee
                ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "gandalf@rivendell.com");

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                employee.setFirstName(""));

        assertEquals(expected, exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"@", "gandalf@", "@rivendell"})
    void shouldNotSetEmailIfIsInvalid(String emails){
        //Arrange
        String expected = "Invalid email";
        Employee employee = new Employee
                ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "gandalf@rivendell.com");

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                employee.setEmail(emails));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotCreateEmployeeWithInvalidEmailNotAnEmail(){
        //Arrange
        String expected = "Invalid email";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "not-an-email"));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotCreateEmployeeWithInvalidEmailOnlyAtSign(){
        //Arrange
        String expected = "Invalid email";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "@"));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotCreateEmployeeWithInvalidEmailOnlyPrefix(){
        //Arrange
        String expected = "Invalid email";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "gandalf@"));

        assertEquals(expected, exception.getMessage());
    }

    @Test
    void shouldNotCreateEmployeeWithInvalidEmailOnlyDomain(){
        //Arrange
        String expected = "Invalid email";

        //Act
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                new Employee ("Gandalf", "The Grey", "Is a Maiar", "Wizard", 24000, "@rivendell"));

        assertEquals(expected, exception.getMessage());
    }
}