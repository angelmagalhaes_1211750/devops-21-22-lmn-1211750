# DEVOPS CA2 PART 1 TECHNICAL REPORT #

## Build Tools with Gradle ##
The goal of the assignment is to use build tools.

Build tools help automate tasks that developers use on a daily basis, like compiling source code into binary code, download dependencies, run tests, and packaging and deployment to production systems. In larger projects, it helps the build process to be more consistent, since there is not need to trying to run commands manually.

This first part of the assignment was developed in an example application using Gradle.

## Requirements
### Analysis
The assignment requires adding dependencies and tasks to a java project using Gradle.
Gradle is an open-source build automation tool, with a rich API for managing projects, tasks, dependency artifact, supporting the automatic download and configuration of dependencies of other libraries. It also has built-in plug-ins for Java and integrates well And and Maven.

In Gradle, Builds consist of one or more projects and each project consists of one or more tasks. A task is a single piece of work, which itself consists of *action* objects. This can include compiling classes, or creating and publishing Java/web archives. By using commands to execute the tasks the root of the project, it should be possible to see the output of the tasks in the console.

The example application implements a basic multithread chat room server.
The following tasks were required to implement in the example application:

1. Add a task to execute the server
2. Add a unit test and update the script to execute it.
3. Add a task to backup the source files.
4. Add a task to make an archive of the source files.

### Implementation
Obtain the example application and experiment with it. This application already comes with gradle included and configured, so the plugin to use Java related tasks is already set:

	plugins {
		// Apply the application plugin to add support for building a Java application
		id 'application'
	}

Executing the `tasks` task allows the display of runnable tasks from the current project:

`./gradlew tasks --all`
 
To execute the gradle build, the recommended way to do it is with the help of the Gradle Wrapper. This Wrapper is a script that invokes a declared version of Gradle. This allows developers to work with a Gradle project without having to follow a manual installation process.

By running `./gradlew build` the .jar file with the application will be built. The following command can be used to execute the server and to run a client:

`java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>`. Substitute <server port> with a valid port number.

Then run the client with `./gradlew runClient`. This task assumes that the chat server's IP is "localhost" and its port is "59001".

#### 1. Add a task to execute the server
To avoid typing the full command to execute the server, a task could be created to execute it. In the build.gradle file, the following script was added:

    task runServer(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches a chat server on localhost:59001 "
        classpath = sourceSets.main.runtimeClasspath
        mainClass = 'basic_demo.ChatServerApp'
        args '59001'
    }

A task named *runServer* was created. By declaring `type:JavaExec` an *enhanced task* is created, that is, a task that already has some functionality built-in, but it is possible to configure it. In this case, the type is *JavaExec*, which executes a Java application in a child process.  

The `classpath` property declares the classpath for executing the main class. As such, the `mainClass` property defines the fully qualified name of the Main class to be executed, while the `args` property defines the arguments passed to the main class to be executed (in this case, it is the port).  

The `group` property is a gradle task configuration that allows grouping of tasks with under the same namespace. It is possible to check a list of all tasks and their groups using `gradle tasks --all`. In this way, when setting a `description` property, it will appear in the same list next to the task name.  

This task has a dependency to the *classes* task, declared with `dependsOn: classes`. This means that the task *classes* will be executed before *runServer*.

After writing the task, the server can then be executed with the following command:  
`./gradlew runServer`.

#### 2. Add a unit test and update the script to execute it.
Gradle supports management of dependencies with other libraries required for the project. Library repositories are defined in the following line of the script:

	repositories {
		mavenCentral()
	}

Since a unit test is to be created, a dependency with *Junit* must be included to the build.gradle script.  
In the `dependencies` block, add the following line:

    dependencies {
        //... other dependencies

        // Use Junit for unit testing
        testImplementation 'junit:junit:4.12'
    }

This will add the Junit framework. The *test* task can be configured with many options, in this case, setting junit as the default testing framework: 

    test {
        useJUnit()
    }

When running the build of a Gradle project, all tests, or the ones specificed in a set of tests will run. To specify tests, this can be done via the configuration of the test task (the best approach is to create an test task instance).

#### 3. Add a task to backup the source files.
Gradle allows to automate backups of any files or directory. This can be done by creating a simple task implementing the *Copy* task.

    task backupSources(type: Copy) {
        from 'src'
        into 'backup'
    }

The `from` defines the source directory of the files to be copied, while the `into` defines the destination directory. If the destination folder doesn't exist, it will be created. 
It is also possible to filter which files can be copied by specifing the types of files from the source directory using `exclude` or `include` properties.  
The copy task can also rename the copied files using the `rename` method, either by setting a pattern to a type of files, or by specifying the name to be changed.

 Run the following command to execute the task:  
 `./gradlew backupSources`

 #### 4. Add a task to make an archive of the source files.
 It is possible to archive a set of files into a compressed file by creating a task that implements the *Zip* task.

    task createSourcesZip(type: Zip) {
        archiveFileName = "src-files.zip"
        destinationDirectory = file("./")
        from "./src"
    }

The `archiveFileName` property sets the name of the compressed file, while the `destinationDirectory` property sets the destination directory for it. The `from` method defines the location of the files to archive.  
This method allows the use of `exclude` and `include` the same way as the *Copy* task would use if there is a need to filter which files are to the archived.

Since a Gradle *task* is a set of *action* objects, it is possible to set actions to perform before or after a specific task. The following lines were added to the script:

    createSourcesZip {
        doLast {
            println "Compressed sources files"
        }
    }

By using the `doLast` method, it creates a task action that print in the console the message after the task is executed. Without it, if the `println` action was at the task level, it would execute at configuration time on every build. Contrary to it, the `doFirst` method will execute the specified actions at the beginning of the task's action list.

 Run the following command to execute the task:  
 `./gradlew createSourcesZip`