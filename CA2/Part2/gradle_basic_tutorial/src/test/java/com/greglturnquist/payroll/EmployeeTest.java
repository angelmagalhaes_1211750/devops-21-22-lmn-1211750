package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void compareTwoSameEmployees(){
        Employee employee1 = new Employee("Gandalf", "The Grey", "Wizard");
        Employee employee2 = new Employee("Gandalf", "The Grey", "Wizard");

        assertEquals(employee1, employee2);
    }

    @Test
    void compareTwoDifferentEmployees(){
        Employee employee1 = new Employee("Gandalf", "The Grey", "Wizard");
        Employee employee2 = new Employee("Saruman", "The White", "Wizard");

        assertNotEquals(employee1, employee2);
    }
}