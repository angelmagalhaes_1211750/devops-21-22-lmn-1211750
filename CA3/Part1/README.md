# DEVOPS CA3 PART 1 TECHNICAL REPORT #

## Virtualization with Vagrant ##
The goal of this assignment is to understand and use virtual machines and virtualization.

In a virtual machine, hardware is simulated by software. Most hardware can be easily simulated by virtualization. Virtual software can be emulated by an hypervisor. This combination of virtual hardware, operating system and software is what is called a *Virtual Machine*.

Virtualization can be done on hardware level (the hypervisor runs directly with the real hardware) or can be hosted by another operating system (hypervisor runs on the host OS).

This has many advantages like providing a restrictable, reproducible environment that can easily be shared with colleagues, and also allowing for the development and deployment to be in sync. Management of resources and maintenance is also a great advantage since replacing *hardware* in a virtual environment can be easily done (unlike real hardware).

## Requirements
### Analysis
The goal of this assignment is to practice with VirtualBox with the projects from the previous assignments running inside the VM.

### Implementation
First experiment will be with the Maven spring boot tutorial from CA1. Second experiment will be with gradle_basic_demo from CA2 Part1.

By following the steps from the lecture, install the VM and the required network utilities (like openssh-server and an ftp server). After installation, connect to the VM via SSH in the host machine by using `ssh angel@192.168.56.6`, replacing the name and the ip with the corresponding user and address.

Next step is to clone the repository to the VM, but for that git is required to be installed with `sudo atp install git`.
Java is also necessary to run the projects, install it with `sudo apt install openjdk-8-jdk-headless`.

#### 1. Spring Boot Tutorial Basic
Our first experiment is with a spring boot tutorial demo using Maven. To be able to run the application, it would be expected that Maven be installed in the VM. This can be done by typing in the command line `sudo apt install maven`. Alternatively, since the project already comes with a maven wrapper included, it is easier to just use the wrapper to run the application. In order to do that, execute permissions must be granted with `chmod u+x mvnw`.

To test the application, run it with `./mvnw spring-boot:run`, then in the host machine test the application by opening the browser and navigate to *http://192.168.56.5:8080*. This corresponds to the address of the VM. With this, the application is currently running in a VM server and can be accessed from another machine.

#### 2. Gradle Basic Demo
The second experiment is with the chat application demo using gradle. Like the previous assignment, since it already comes packed with a wrapper, it is easier to just grant execute permissions to it: `chmod u+x gradlew`. After this, run `./gradlew build` to download the required dependencies.

Launch the application server by running the gradle task `./gradlew runServer`.

The second part of the experiment cannot be done in the VM, since it requires a graphical interface to execute. The current VM is just a server without GUI, so the task cannot be executed. Trying to run the application client will just launch a java exception.

With the server running in the VM, change back to the host machine and run the client in the local project with `./gradlew runClient`. The connection will be refused because the task is currently set to connect to the localhost. Since the server is currently running in a VM, the task should be updated to connect the VM ip address *192.168.56.5*. With this update, by running the task again, the host machine will successfully connect to the server in the VM and will pop-up the client interface.

This has the advantage of allowing an application server to be hosted in a remote server and the client to be distributed with whoever will use it, just by setting the correct ip address. With the server running in a VM, changes to the application that would impact the machine are much easier to perform.