# DEVOPS CA3 PART 2 TECHNICAL REPORT #

## Virtualization with Vagrant ##
The goal of this assignment is to understand and use virtual machines and virtualization.
For this part of the assignment, Vagrant is used to configure and manage multiple VM.

Vagrant is an open-source tool that helps us automate the creation and management of virtual machines. By specifying the configuration of the virtual machine in a simple configuration file, creating the same virtual machine with just one command.

Vagrant allows the configuration of the setup of the virtual machine, along with all the necessary components required for applications to be run by the VM, like webservers or Java. Instead of setting up all the components manually, all the required software components and their configuration information are specified in the configuration file. This makes it easier to reproduce enviroments, since all that's needed to share a VM is to share the Vagrant configuration file.

## Requirements
### Analysis
The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment to execute the tutorial spring boot application, gradle "basic" version from previous CA2 Part 2 assignment.

Vagrant will configure 2 virtual machines:

* web: this virtual machine will be used to run *tomcat* and the basic spring boot application.
* db: this virtual machine will be used to execute the *H2* server database.

### Implementation
Vagrant sets up the configuration of VM in a file called *Vagrantfile*. Vagrant sets up the VM by *provisioning* with necessary requirements.
To start a new Vagrantfile, run the command `vagrant init envimation/ubuntu-xenial`. This will setup a vagrant project based on ubuntu 16.04 box. A vagrant box is a clone of a base operating system image, which speeds up the launching and provisioning process. It is also possible to just download the box and store it locally with `vagrant box add envimation/ubuntu-xenial`.

For this assignment, a **Vagrantfile was already provided**, so there is no need to set it up with the previous commands. The Vagrantfile is already configured with the following provisions:

* A common provision for all the VM:

        config.vm.provision "shell", inline: <<-SHELL
            sudo apt-get update -y
            sudo apt-get install iputils-ping -y
            sudo apt-get install -y avahi-daemon libnss-mdns
            sudo apt-get install -y unzip
            sudo apt-get install openjdk-8-jdk-headless -y
        SHELL

* The following provision sets up the VM for the **db** VM.
  * Network configuration:

        db.vm.network "private_network", ip: "192.168.56.11"
        db.vm.network "forwarded_port", guest: 8082, host: 8082
        db.vm.network "forwarded_port", guest: 9092, host: 9092

  * Download and install dependencies:

        db.vm.provision "shell", inline: <<-SHELL
            wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
        SHELL

        db.vm.provision "shell", :run => 'always', inline: <<-SHELL
            java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
        SHELL

* The following provision sets up the VM for the **web** VM.
    * Network configuration:
  
            web.vm.network "private_network", ip: "192.168.56.10"
            web.vm.network "forwarded_port", guest: 8080, host: 8080

    * More RAM is needed to run tomcat, so vagrant can configure it:

            web.vm.provider "virtualbox" do |v|
                v.memory = 1024
            end

    * Download and install dependencies
    
            web.vm.provision "shell", inline: <<-SHELL, privileged: false
                sudo apt-get install git -y
                sudo apt-get install nodejs -y
                sudo apt-get install npm -y
                sudo ln -s /usr/bin/nodejs /usr/bin/node
                sudo apt install tomcat8 -y
                sudo apt install tomcat8-admin -y
            SHELL


As stated, provisining allows the installation of dependencies and configuration of the operating system on install.  
The following provision allows the download of an application from a repository, running a gradle build and copying the built .war file to tomcat server to deploy the application:

    git clone https://angelmagalhaes_1211750@bitbucket.org/angelmagalhaes_1211750/devops-21-22-lmn-1211750.git
    cd devops-21-22-lmn-1211750/CA3/Part2/gradle_basic_tutorial
    chmod u+x gradlew
    ./gradlew clean build
    sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

*NB: this configuration was changed from the provided Vagrant file to download the current user repository and following the structure of the copied project from previous assignment.*

#### Unexpected issues
When copying the project from the repository some errors ocurred. Building and deploying the project was not possible due to wrong configuration from the project:

* The project was configured to use Java version 11. The installed version of java was `openjdk-8-jdk-headless`. Unable to update the java version of the installed vagrant box, the solution found was to change the configuration of the project to use the version of java installed in the guest OS.
* The project didn't have support for building *.war* files, so the configuration was updated to support it. A dependency to the deployment for tomcat was also added.

#### Solving issues
Due to the unexpected issues, several commands helped debug and solve the issues:

* Vagrant allows command line ssh connection. By typing `vagrant ssh web`, it allowed access to the *web* VM. With this, it was clear that the build of the project was not being executed. This also allowed to remove the downloaded repository with wrong configuration.
* While trying to setup the project, some errors ocurred that "locked" the folder and didn't allowed the deletion of it. Unable to fix this issue, and to be able to download the new changes made to the repository, the solution found was to re-install the VM. Vagrant allows a quick and simple way to remove them by just using the command `vagrant destroy -f`. This command completely removes the VM files from the host machine, but it leaves the Vagrantfile intact, allowing a quick and fresh install of the virtual machines with the same configuration.

#### Running the application
Before starting the VM, the spring boot application should be updated with the correct configuration so that the *db* VM hosts the H2 server, by following the steps provided in the commits found in https://bitbucket.org/atb/tut-basic-gradle/commits/. By setting up the spring *data source* to the address of the *db* VM (192.168.56.11:9092) along with other configurations and commiting the changes to the repository, it was now possible to start the VM.

To start the vagrant VM, type the command `vagrant up`. On first setup vagrant already provisions the VM with the necessary configurations.

If changes to the Vagrantfile provisions are made, it should be re-provisioned with `vagrant provision`, otherwise, next time the VM boots it will have the previous configuration. This can also be done by re-starting the VM using `vagrant reload --provision`.

It should now be possible to access *http://192.168.56.10:8080/demo-0.0.1-SNAPSHOT/* and see the application running.

To stop the VM, type the command `vagrant halt`. Also, as previously stated, the VM can be completely removed with `vagrant destroy -f`. The vagrant box can also be removed from the local filesystem with `vagrant box remove envimation/ubuntu-xenial`.


*****
## Alternative Solution: VMWare Workstation

### Analysis
VMware Workstation Player is a free virtualization software available for non-commercial use. To be able to use for commercial use, a license must be paid. It is used for managing and creating virtual machines but works best when running a single VM.

Comparing to VirtualBox, both are free to use and provide the same features. Unfotunately, VMWare requires a license to use some features like Snapshots. One of the advantages VMWare has over VirtualBox is its performance, since VMWare's virtual machines run faster. This is important for large-scale or Enterprise projects that require high performance machines.

### Implementation
To use VMWare Workstation with Vagrant, the VMWare utilty must be installed. After installation, fetch the plugin with `vagrant plugin install vagrant-vmware-desktop`.

To run the previous setup, some changes must be made to the Vagrantfile, so that the network configuration doesn't collide with the previous boxes.

* For *db* VM: `db.vm.network "private_network", ip: "192.168.33.11"`
* For *web* VM: `web.vm.network "private_network", ip: "192.168.33.10"`

It is also necessary to specify the provider and for the *web* VM the configuration for the memory:

    config.vm.provider "vmware_desktop" do |v|
        v.gui = true
    end

    web.vm.provider "vmware_desktop" do |v|
        v.vmx["memsize"] = "1024"
        v.gui = true
    end

After configuration, a copy of the project was pushed to the remote repository with the application properties necessary changes to the database ip address. The provision related to the change of directory was also updated to the alternative directory.

### Conclusion
Working with Virtual Machines allows more flexibility to the deployment of applications. Vagrant allows a much simpler configuration for the VM, which helps in making an easier and faster setup.

On the scale of the projects in which I experimented, there are not many notable differences between VirtualBox and VMWare, since both work as expected and didn't required extensive configuration.