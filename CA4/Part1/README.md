# DEVOPS CA4 PART 1 TECHNICAL REPORT #

## Containers with Docker ##

The goal of this assignment is to understand and learn how to use and create docker images and run containers.

A container is a process on a machine that has been isolated from all the other processes on the host machine. It is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.

When running a container, it uses an isolated filesystem. This custom filesystem is provided by a container image. Since the image contains the container's filesystem, it must contain everything needed to run an application - all dependencies, configuration, scripts, binaries, etc. The image also contains other configuration for the container, such as environment variables, a default command to run, and other metadata. Container images become containers at runtime.

It works similar to a virtual machine, but while VMs are abstractions of physical hardware, containers are an abstraction at the app layer that packages code and dependencies together. Containers take less space than VMs, can handle more applications and require fewer VMs and operating systems.

## Requirements ##

### Analysis ###

For this part of the assignment, a container image will be created and run using Docker for the chat application from CA2.
A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings. Docker images become containers when they run on Docker Engine. Available for both Linux and Windows-based applications, containerized software will always run the same, regardless of the infrastructure. Containers isolate software from its environment and ensure that it works uniformly despite differences for instance between development and staging.

### Implementation ###

The goal is to be able to package and execute the chat server in a container.

Docker images can be pulled from remote repositories or configured through the command line.
The configuration of Docker images can be automated using Dockerfiles.A Dockerfile is a text-based script of instructions that is used to create a container image.

#### 1. Configuring the image ####
Start by creating a file **Dockerfile** (without any file extension). First define which base image with the `FROM` instruction:

`FROM ubuntu:18.04`

The install the tools and applications required to the container with the `RUN` instruction. This is used to execute commands while building the image. Each of the `RUN` layer runs independently from one another.

    RUN apt-get update -y
    RUN apt-get install openjdk-8-jdk-headless -y
    RUN apt-get install git -y

The previous lines will install Java JDK8 and Git, required to run the application and to clone the application from the remote repository.

Since `RUN` can't be used to change directories due to being independent layers, the `WORKDIR` instruction will set the working directory for any instruction that follow it in the Dockerfile. If the `WORKDIR` doesn't exists, it will be created even if not used in subsequent instructions. Absolute or relative paths can be specified, being that the relative is to the one previously specified in a previous `WORKDIR` instruction:

`WORKDIR /usr/repo`

After specifying the directory, we can then clone the repo and change once again to the directory with the following instructions:

    RUN git clone https://angelmagalhaes_1211750@bitbucket.org/angelmagalhaes_1211750/devops-21-22-lmn-1211750.git
    WORKDIR /usr/repo/devops-21-22-lmn-1211750/CA2/Part1/gradle_basic_demo

Then just specify the remaining commands to build the application and generate the necessary files for the application to run:

    RUN chmod u+x ./gradlew
    RUN ./gradlew clean build


In order to access the application, the `EXPOSE` instruction must be specified. `EXPOSE` informs Docker that the container listens on the specified network ports at runtime. This instruction does not actually publish the port. It functions as a type of documentation between the person who builds the image and the person who runs the container, about which ports are intended to be published. The Dockerfile will inform docker to listen on the following port:

`EXPOSE 59001`

Finally, we must specify which default command(s) will run when executing the container with the `CMD` instruction. There can only be one `CMD` instruction in a Dockerfile. If you list more than one `CMD` then only the last `CMD` will take effect. There are several forms to specify the instruction, either in *shell* form, or *exec* form. The following instruction was added in *exec* form, which is the preferred form:

`CMD [ "./gradlew",  "runServer" ]`

This instruction will execute the runServer gradle task (which starts the server on port 59001).

#### 2. Building the image ####

After completing the Dockerfile the image can be built through the command line. Change directory to where the Dockerfile is located and run the following command:

`docker build -t chat-server-a .`

On first build, the base image will probably have to be downloaded if it is not already on the system. It then proceeds to execute the statements defined in the file. The `-t` flag tags the image with a human-readable name for the final image. This way, we can refer to `chat-server-a` when we run the container. The `.` at the end of the command tells that Docker should look for the Dockerfile in the current directory.

The image can now be seen in the docker's image list with the following command:

`docker images`

If part of the application is to be updated, it means that the image must also be updated. To do that, the `build` command must be executed again. Most instructions from the Dockerfile are probably cached, so if necessary, the `--no-cache` option can be specified to allow the execution of the instructions once again.

#### 3. Starting the application ####

Since an image has already been created, we can run the application in a container with the following command:

`docker run -p 59001:59001 chat-server-a`

The `-p` flag tells Docker to create a mapping between the the host's port 59001 and the container's port 59001. Without this mapping, the application cannot be accessed. Now with local access to the application's client, if the gradle's *runClient* task is configured to connect to *localhost:59001* , then it is possible to the connect to the chat server.

Other options to `run` can also be specified, like `-d` which runs the container *detached* (in the background) or `--name`, which allows to specify a name for the container, instead of letting docker randomize it.

To check the list of the running containers, type:

`docker ps`

This will display information about the running containers, including the name and **container id**. With the container id (or name), it is possible to perform other docker commands, like opening a ssh session through another terminal:

`docker exec -it container-id bash`

The `exec` command allows to execute commands in the specified container. In this case, we want to open a bash terminal inside the container.


#### 4. Stopping and removing the container ####

Now that the application is running, it can be stopped through another terminal window (since the current one is running the application). By running `docker ps -a` to display **all** existing containers (not just the ones currently running) it should be possible obtain the container id so the container can be stopped:

`docker stop container-id`

Since containers are transient by nature, the changes made to a running container are not saved to the image, meaning that when the container is shut-off, all changes are lost.
The container can then be started again if necessary with:

`docker start container-id`

When the container is no longer necessary, it can be removed with:

`docker rm -f container-id`.

Only stopped containers can be removed, so the `-f` flag forces the container to stop in case it is running.

The image can also be removed with `docker rmi image-name`.


#### 5. Tags and pushing to Docker Hub.

Docker Hub is the default registry where all the base images have come from. It also allows the sharing of docker images.

First step is to access [Docker Hub](https://hub.docker.com/), log in (or create an account), and create a public repository. (ex. `dangelsanchez/devops-ca4*`) 

It is also necessary to tag the local images to be shared to the repository. By using the `docker tag` command, we can create a *TARGET_IMAGE* that refers to a *SOURCE_IMAGE*.

First, log in to Docker Hub with `docker login -u your-username`. Then, use the `tag` command to give the `chat-server-a` image a new name, like the following:

`docker tag chat-server-a dangelsanchez/devops-ca4:chat-server-a`

Then push the image to the repository with:

`docker push dangelsanchez/devops-ca4:chat-server-a`


**The images can be found in:**

https://hub.docker.com/r/dangelsanchez/devops-ca4/tags

#### 6. Exploring further ####

One of the options to explore the possibilities of docker was to create another image that, instead of cloning the entire application, it should just copy the `.jar` file to the container.

To do this, the Dockerfile necessary will be almost the same as the first version, with minor differences. No need to install git and related commands (like cloning and granting permissions) and the `CMD` instruction will also change.

The `EXPOSE` instruction should also change the port to `59057`

Then, it should be specified that we want to copy a file from the host to the container. We do that with the `COPY` instruction. By placing the `.jar` file at the same directory as the Dockerfile, we can just type copy with the following instruction:

`COPY ./basic_demo-0.1.0.jar /usr/repo` (provided that the /usr/repo is a valid directory in the container).

Then, finish the instructions with the command to run the application server:

`CMD java -cp /usr/repo/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59057`

Build the image and run the container:

    docker build -t chat-server-b .
    docker run -dp 59057:59057 chat-server-b


**On a personal note**, I encountered some problems with this approach. Since the `.jar` file was built using a newer version (11) of Java than the one in the container (8), errors kept ocurring while building the application. Changing the JDK install instruction was also giving me errors while downloading the jdk, so I took another approach:

* Run the initial container and access it's terminal with: `docker exec -it container-id bash`.
* Obtain the application `.jar` directory.
* Leave the container terminal with `exit`.
* Execute `docker cp da460b5d66b3:/usr/repo/devops-21-22-lmn-1211750/CA2/Part1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar ./basic_demo-0.1.0.jar`

The command on the last step copies the file specified in source path (the container's .jar directory `da460b5d66b3:/usr/repo...`) and the destination path (current folder `./`).

This way, the `.jar` java version will be the same as the containers and the container will run.

This solution was also tagged and pushed to Docker Hub repository with:

    docker tag chat-server-b dangelsanchez/devops-ca4:chat-server-b
    docker push dangelsanchez/devops-ca4:chat-server-b

**Docker Hub Link:**
https://hub.docker.com/r/dangelsanchez/devops-ca4/tags