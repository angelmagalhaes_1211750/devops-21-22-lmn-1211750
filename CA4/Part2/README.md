# DEVOPS CA4 PART 2 TECHNICAL REPORT #

## Multiple Containers with Docker ##

The goal of this assignment is to understand and learn how to use and create docker images and run containers.

A container is a process on a machine that has been isolated from all the other processes on the host machine. It is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.

Even if containers are isolated, they still need to comunicate to the external world and provide services. They might also need to talk to other containers in the same host or even across hosts.
We can even split an application in multiple containers, say the split the application core from the database. A multi-container app provides many advantages:

* Able to scale the APIs and frontends differently than the databases.
* Updating and versioning in isolation.
* A container with a local database can be used for development and a differente database for production.
* Since a container only starts one process, it is easier to manage than using just the one container.

Each container should do one thing and do it well.

## Requirements ##

### Analysis ###

The goal of this assignment is to use Docker to setup a containerized environment to execute your version of the gradle version of the spring basic tutorial application.  
For that purpose, we will use Docker Compose.

Docker Compose is a tool that was developed to help define and share multi-container applications. With Compose, we can create a YAML file to define the services and with a single command, can spin everything up or tear it all down.

Use docker compose to produce two containers:

* **web**: container that will run *Tomcat* and the spring application.
* **db**: container that will execute the *H2* server database.

#### Container Networking ####

By default, containers run in isolation and don't know anything about other processes or containers in the same machine. We will use the concept of **networking** to allow several containers to communicate. To do that in Docker, we can use what we call a *bridge newtork*, a private network within a single docker host. Containers withing the same network can communicate while still being isolated from other containers not connected to that bridge network.

To create a network, run `docker network create network-name`. A running container can then be added to the network with `docker network connect network-name container-id`, or when initially run with `docker run --network network-name image-id`. Other commands must also be specified to configure the network like specify all of the environment variables, expose ports, and more.

For communication among containers running on different Docker daemon hosts, you can use an overlay network.

#### Container Volumes ####
While containers can create, update, and delete files, those changes are lost when the container is removed and all changes are isolated to that container. Volumes provide the ability to connect specific filesystem paths of the container back to the host machine. If a directory in the container is mounted, changes in that directory are also seen on the host machine. If we mount that same directory across container restarts, we'd see the same files.  
Volumes are completely managed by Docker, easier to back up and migrate, can be more safely shared among multiple containers and even let you store volumes on remote hosts or cloud providers.

We can use the concept of Volumes to store the information to be saved in the application database. To create a volume, run `docker volume create volume-name`. Then, to run a container using this volume `docker run -db 8080:8080 -v volume-name:/etc/files image-name`. Using the `-v` with the volume name will specify which volume to mount, then select the filepath from the container to store in the volume (ex. /etc/files).

### Implementation ###

Executing all the commands required to set up a networking and volumes can be a bit overwhelming, so we can use Docker Compose to help setting up multiple containers.

The big advantage of using Compose is you can define your application stack in a file and keep it at the root of your project repo, since it can be version controlled. This allows easy contribution to the application since all it takes is cloning the repository and start the compose app.

#### 1. Create the compose file ####
At the root of the project, create a file called `docker-compose.yml`. The first line should be the Compose file format version:

`version: "3"`

Then we define the *Services* we want to run as part of the application. Services can also be interpreted as Containers that do just one thing.

`services:`

As stated earlier, two services will be defined, starting with the **web** service:

    web:
      build: web
      ports:
        - "8080:8080"
      networks:
        default:
          ipv4_address: 192.168.56.10
      depends_on:
        - "db"

Then define the **db** service:

    db:
      build: db
      ports:
       - "8082:8082"
       - "9092:9092"
      volumes:
       - ./data:/usr/src/data-backup
      networks:
       default:
         ipv4_address: 192.168.56.11

Lets check on each of the properties:

* build: Defines which container for this service will be built using the configuration inside a folder called *web*. In the folder is a Dockerfile with the container configuration.
* ports: Maps the container port to a port on the host (host-port:container-port). This must also be configured in the Dockerfile.
* networks: Defines the networks that the service is attached to, referencing entried under the top-level networks key (more ahead).
* depends_on: expresses startup and shutdown dependencies between services.
* volumes: Defines name volumes that must be accessible by the service container. Using the short syntax can be defined as `VOLUME:CONTAINER_PATH`. The volume may either be a host path or a volume name. The container_path is the path where the volume is mounted.

Finally, we must set the top-level network.

    networks:
      default:
        ipam:
          driver: default
          config:
            - subnet: 192.168.56.0/24

* ipam: Specifies the custom IPAM (IP address management) configuration.
* driver: Will use a custom IPAM driver (in this case will be the default).
* config: A list of configuration elements like subnet, gateway and ip range.
* subnet: The subnet in CIDR format the represents a network segment.

The Dockerfile in web should clone the repository, change the working directory to the last class assignment (Vagrant), give permissions to the gradle wrapper, build the application and finally, copy the .war file to the tomcat deployment file path (as done in the previous assignment with Vagrant).

The Dockerfile in db should get the .jar for the H2 server database and run it.

Now, the file structure will look like this:

    app
    +---data
    +---db
    |   +-- Dockerfile
    +---web
    |   +-- Dockerfile
    +---docker-compose.yml   


#### 2. Get it all running ####

To start several services in a Docker Compose, just change to the root directory (where the docker-compose.yml file is) and run:

`docker compose up`

This command will build the images from the Dockerfiles, pull the images from registries, create and start containers and configure the bridge network for the containers. Now it is possible to check all running composed containers with `docker compose ps`.

There are other commands that can be used with Compose, like:

* `docker compose build`: build the images.
* `docker compose start [service-name]`: start all or a single container (or stop if using `stop`).
* `docker compose rm [service-name]`: remove all or a single stopped container.
* `docker compose image`: lists the compose images.
* `docker compose logs service-name`: see the logs generated by the container.

Now that the containers are running, we can access the web application by accessing: http://localhost:8080/demo-0.0.1-SNAPSHOT.

Access to the H2 server database console can be accessed through http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console. For the connection string, we can replace the IP address with the container's service name, **db** in this case: `jdbc:h2:tcp://db/./jpadb`


#### 3. Backup the database ####

In order to backup the database from the db container, we can store it in the docker-compose.yml file specified Volume.

With the containers running, we can execute commands through Compose to access the container through a bash terminal:

`docker compose exec db bash`

Now we can just execute a copy command to the specified container_path and copy the `.db` file in the root directory:

`cp jpadb.mv.db /usr/src/data-backup`

Now by exiting the container's terminal with `exit` we can see that the `jpadb.mv.db` has been copied to the /data folder in our application.

#### 4. Pushing to Docker Hub ####

Like the previous part of this assignment, we must tag the images so they can be pushed.

`docker tag part2_web dangelsanchez/devops-ca4:part2_web`
`docker tag part2_db dangelsanchez/devops-ca4:part2_db`

After tagging, we can make changes to the `docker-compose.yml` file to specify which image we want to use with `image:`. We will specify the tagged image for each Service: 

    web:
        image: dangelsanchez/devops-ca4:part2_web

    ### other configs

    db:
        image: dangelsanchez/devops-ca4:part2_db
    

All that is required after that is push to Docker Hub with `docker compose push`.

**Docker Hub Link:**
https://hub.docker.com/r/dangelsanchez/devops-ca4/tags


*****
## Alternative Solution: Deployment to Heroku

Heroku is a cloud service platform that enables simple application development and deployment.
The Heroku cloud service platform is based on a managed container (called dynos within the Heroku paradigm) system. It has integrated data services and a powerful ecosystem for deploying and running modern applications.

It supports many programming languages like Ruby, Java, Node.js, Scala, Clojure, Python, PHP, and Go.

Heroku is a Platform as a Service that sits on top of AWS (Amazon Web Services) to provide an experience that is specifically designed to help developers.

Heroku runs his apps in dynos - virtual machines that can be powered up or down based on the application size. If you want to process more data or run more complex tasks, you need to add more blocks (dynos) or increase the size of the blocks.

### Analysis and Implementation ###

First install the heroku Command Line Interface (CLI). This will help you manage and scale the applications, provision add-ons, view logs and run the application locally.

After installing, login with `heroku login`, which will prompt a browser window. Then login to the container registry `heroku container:login`.


#### Encountered problems ####
After several (failed) experiments trying to deploy the last version of the application, it quickly became overwhelming. The best approach for experimenting with Heroku is to use the basic version of the app, with the database running alongside the application, having the need to only have one container.

#### Solution ####
On the application root create the Dockerfile with similar configuration as the first part of the assignment.

One problem encountered was that the cloning of the repository and the changing of directories was not working as expected, so instead the application was build locally and the `.war` file was copied to the image through the dockerfile with the following statement:

`COPY ./demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/`

Another problem encountered is that Heroku doesn't support the `EXPOSE` command, so there is no way to be certain that tomcat would use the port 8080. As per the teacher's suggestion, the tomcat loading should use the dynamic port, therefore, a bash script called *catalina-dynamic-port.sh* was created with the following instructions:

    #!/bin/bash
    echo $PORT
    sed -i 's/port="8080"/port="'${PORT}'"/' /usr/local/tomcat/conf/server.xml
    /usr/local/tomcat/bin/catalina.sh run

The script replaces the line *port="8080"* with the dynamic port set in the enviroment variable at *$PORT* at the server.xml tomcat configuration file. Then, it executes the catalina servlet specification.

Then, we can run change the container with these changes by adding the following instructions to the Dockerfile:

    ADD ./catalina-dynamic-port.sh /usr/local/tomcat/bin/catalina-dynamic-port.sh
    RUN chmod a+x /usr/local/tomcat/bin/catalina-dynamic-port.sh
    CMD ["catalina-dynamic-port.sh"]

The bash script was added to the to the bin tomcat folder, granted execute permission, then finally it is executed with the `CMD` instruction.

#### Deployment ####
With this configuration, we can finally deploy the application to Heroku.

To create a Heroku app, go to the app's directory and enter `heroku create`. A new app will be created with a randomized name.

Heroku allows the building of Docker images directly and push it to the Container Registry. If the app's directory contains the Dockerfile, run:

`heroku container:push web --app floating-hollows-08175`

`web` is a *process type* which tells Heroku which one or more **dynos** are to be instantiated. The web dynos can receive HTTP traffic from routers and typically run web servers.  
`--app floating-hollows-08175` indicates which app to push (the randomized named given at `create`).

After this command, it can then be released to Heroku with the following command:

`heroku container:release web --app floating-hollows-08175`

Same as the previous command, but just change `push` with `release`.

The application was successfully deployed to Heroku. To open it, run:

`heroku open --app floating-hollows-08175`

Probably the tomcat's 404 page will be displayed. Navigate to the following link to see the application running:

### DEPLOYED APPLICATION ###

https://floating-hollows-08175.herokuapp.com/demo-0.0.1-SNAPSHOT/

Test the application with a POST request:

    curl -X POST https://floating-hollows-08175.herokuapp.com/demo-0.0.1-SNAPSHOT/api/employees -d "{\"firstName\": \"Gandalf\", \"lastName\": \"The Grey\", \"description\": \"Wizard\"}" -H "Content-Type:application/json"

A new entry was added to the table and the application works as expected.

### Conclusion ###
Heroku makes deployment of container images a much simpler tasks. Despite the problems encountered, it was possible to create a docker image and deploy the container for a web application to Heroku with some some small changes. From reading the documentation, the spring application could be deployed and then connected to a PostgreSQL database through a plugin add-on.

It is also possible to deploy apps directly without the need of a container. By having a project in a git repository and cloning the repository, when deploying to heroku starting with `heroku create`, it will create the app and a Git remote associated to the repository. Then, with `git push heroku main` it will build the application according to the dependencies in the pom.xml file and deploy it.