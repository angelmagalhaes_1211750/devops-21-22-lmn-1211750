# DEVOPS CA5 PART 1 TECHNICAL REPORT #

## CI/CD Pipelines with Jenkins ##

The goal of this assignment is to understand and learn how to create a CI/CD pipeline using Jenkins.

**CI** (Continuous Integration), is a set of practices that drive development teams to implement code changes and frequently check them to a version control repository. It establishes an automated way to build, package and test the applications. By having a consistent integration process, developers commit their code more frequently, leading to a better collaboration and code quality. CI also puts a great importance on testing automation to check that the application is not merged in the main branch.

**CD** (Continuous Delivery), is the follow-up for CI, since it is an automated way of deploying the application code to selected environments like production, development or testing.

By setting up delivery *pipelines* with all the necessary steps, it creates an efficient and repeated process to develop software, allowing organizations to deliver new features faster and efficiently.
These pipelines usually package all the software and database components, but the automation will also execute all necessary tests. Most CI/CD tools allow the the kick off builds on demand, triggered by code commits in the version control repository, or by a defined schedule.

These are some benefits for CI/CD practices:

* Less bugs are shipped to production since they are captured early by automated testing.
* Easy to build a release. If a build is broken they get alerted as soon as it happens and can work on fixing it.
* Deployment complexity is reduced and can be deployed more often, accelerating feedback from customers.

## Requirements ##

### Analysis ###

The goal of this part of the assignment is to create a simple pipeline using Jenkins.

Jenkins is a free and open-source automation server. It helps automate the parts of software development related to building, testing, and deploying, facilitating Continuous Integration and Continuous Delivery. Jenkins provides many plugins to integrate with test automation tools and frameworks, helping run test suites, gather and dashboard results and provide details on failures.

Provisioning is the process of setting up IT infrastructure. Once something has been provisioned, the next step is configuration. Jenkins supports Jenkins as Code (Configuration as Code). The ‘as code’ paradigm is about being able to reproduce and/or restore a full environment within minutes based on pre-programmed configurations (called recipes in Jenkins) and automation and which is managed as code. This allows configuration to be done via a simple, human-friendly, plain-text syntax through a *Jenkinsfile*.

The project to be configured is the one from CA2 Part1 (chat application).  
The pipeline will consist in the following stages/tasks:

* Checkout
* Assemble
* Test
* Archive

### Implementation ###

#### 1. Install Jenkins ####
Jenkins is a Java application packaged as a *.war* file, so it can be directly run by executing the command `java -jar jenkins.war`.  
An alternative to this is to run Jenkins through a container. There is a recommended official Docker image to use [Jenkins Blue Ocean](https://hub.docker.com/r/jenkinsci/blueocean/) image.

The following steps to install Jenkins as a container can be found on the following link: [Installing Jenkins with Docker](https://www.jenkins.io/doc/book/installing/docker/). Nevertheless, I will provide the instructions to be executed in the terminal (Windows commands) since it was the option I selected:

* Create a bridge network with `docker network create jenkins`
* To use Docker commands inside the Jenkins container, we must connect it to another container which contains only Docker

        docker run --name jenkins-docker --rm --detach ^
        --privileged --network jenkins --network-alias docker ^
        --env DOCKER_TLS_CERTDIR=/certs ^
        --volume jenkins-docker-certs:/certs/client ^
        --volume jenkins-data:/var/jenkins_home ^
        --publish 2376:2376 ^
        docker:dind

This will setup the network connection for this container, map the necessary volumes and download the `docker:dind` image. The `--rm` flag will remove the container when stopped, so the previous command must be executed again if docker commands are necessary.

* Customise the official Jenkins Docker image with the following Dockerfile:

        FROM jenkins/jenkins:2.332.3-jdk11
        USER root
        RUN apt-get update && apt-get install -y lsb-release
        RUN curl -fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
        https://download.docker.com/linux/debian/gpg
        RUN echo "deb [arch=$(dpkg --print-architecture) \
        signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
        https://download.docker.com/linux/debian \
        $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
        RUN apt-get update && apt-get install -y docker-ce-cli
        USER jenkins
        RUN jenkins-plugin-cli --plugins "blueocean:1.25.3 docker-workflow:1.28"

* Then build the image with `docker build -t myjenkins-blueocean:2.332.3-1 .`
* Finally, run you own image with:

        docker run --name jenkins-blueocean --restart=on-failure --detach ^
        --network jenkins --env DOCKER_HOST=tcp://docker:2376 ^
        --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 ^
        --volume jenkins-data:/var/jenkins_home ^
        --volume jenkins-docker-certs:/certs/client:ro ^
        --publish 8080:8080 --publish 50000:50000 myjenkins-blueocean:2.332.3-1

After following all the previous steps, the [setup wizard](https://www.jenkins.io/doc/book/installing/docker/#setup-wizard) will start. Follow the instructions to complete the installation.

#### 2. Setting up the pipeline ####
##### 2.1 Creating the required Job #####
A job can be considered as a particular task to achieve a required objective in Jenkins. Moreover, we can create as well as build these jobs to test our application or project.

To create a job in Jenkins, in the Jenkins dashboard click on the "**New Item**" option. In the following page, fill the name of the job (*CA5-part1*) and select the *type* of job. The type can be anything from a particular task or several, but since we want to create a pipeline, select the **Pipeline** option.  
After creating the Job, the configuration page will be displayed.

In the configuration page we can set up general options for the build, and the pipeline to be executed. In the pipeline section, we can define the script either by entering the script in the specific area, or through SCM (Source Control Management). Since we will choose the latter, we must first configure and push the required file to the repository.

##### 2.2 Setting up repository credentials ######
Since we want Jenkins to have access to a private repository, credentials must be stored in Jenkins and then specify which ones are to use.

In the Jenkins Dashboard, go to "**Manage Jenkins**", in the *Security* section, go to **Manage Credentials**. Access the *System* then *Global* credentials and then click n *Add Credentials*. Fill in the details of username and password (if using Bitbucket as SCM, then it is an *app password* that should be provided). 
 Then enter an **ID** for this password. This will be the *ID* to which it will be referenced on the pipeline script, so it's important to be a meaningful name (*ASM-bitbucket-credentials*). A description is also a nice to have, since it can help identify it on other Jenkins' configuration pages (like the job configuration page).

 ##### 2.3 Create the Jenkinsfile script #####
 Start by creating a Jenkinsfile at the root of the project (*CA2/Part1/gradle_basic_demo*).

 We will write a *Declarative Pipeline*. It provides a more simplified and opinionated syntax. All declarative pipelines code must be enclosed within the following block:

    pipeline {
        /* pipeline instructions here */
    }

The pipeline will contain one or more *Sections*. We start by defining the *agent*. The agent is tasked to run a job, and in our case we will define where the entire pipeline will execute the Jenkins environment. Declare the agent with the following line: `agent any`. This will execute the pipeline on any available agent. 

We then define the `stages` section, where the *bulk* of the work will be located. At minimum, there should be at least one `stage` directive for each part of the continuous delivery process. For each `stage` we then define the `steps`, which will be executed at the given stage.  
Each *step* is like a single command which performs a single action.  When a step succeeds it moves onto the next step. When a step fails to execute correctly the Pipeline will fail.

So far we have:

    pipeline {
        agent any
        stages {
            /* define stages here */
        }
    }

###### 2.3.1 Checkout Stage ######
The following stage will checkout the code from the repository. To do that, we can use the **git** step. For this step we must provide it with the credentials id (which we created earlier) and the repository url to checkout. We can then add the stage as following:

    stage ('Checkout') {
        steps {
            echo 'Checking out...'
            git credentialsId: 'ASM-bitbucket-credentials', url: 'https://bitbucket.org/angelmagalhaes_1211750/devops-21-22-lmn-1211750'
        }
    }

We define the name of the stage as *Checkout*, then specify the steps. We also added the **echo** step to provide more info to the console output when running the job.

###### 2.3.2 Assemble Stage ######
This stage will compile and produce the archive files for the application. In order to do that, we can run the gradle tasks from the project. Since the repository has several folders and projects, we must tell where the gradle wrapper for the specific project is located. We can use the **dir** step to change the current directory. Any step executed iniside the block will use the defined directory as current and any relative path will use it as base.

    stage ('Assemble') {
        steps {
            echo 'Assembling...'
            dir('CA2/Part1/gradle_basic_demo') {
                sh '''
                    chmod u+x gradlew 
                    ./gradlew clean assemble
                '''
                }
        }
    }

We then use a step specific for the Pipeline: Nodes and Processes plugin (which should be installed by default if following the recommended steps from the install wizard).  

The **sh** command will run a Bourne shell script. Since we are running Jenkins in a linux container, we can use this script, otherwise we would be using the **bat** for the Windows Batch Script.  
We then execute the required commands (using the multiline with `'''`) to run the gradle clean and assemble tasks. No tests are run using this commands, since that is a task for the next stage.

###### 2.3.3 Test Stage ######
In this stage, we want to run the test suite and publish the results to Jenkins. For that, we can use the JUnit Plugin's **junit** step.

    stage ('Test') {
        steps {
            echo 'Testing...'
            dir('CA2/Part1/gradle_basic_demo') {
                sh './gradlew test'
                junit 'build/test-results/**/*.xml'
            }
        }
    }

As before, we change the current directory so we can run necessary steps in the appropriate folder. We start by running the gradle task for the test so it can generate the reports. Then, we use the **junit** step to publish the test results to Jenkins. We specify the path of the test report files to add, in this case it's all the folders under the test-results directory and all files with the .xml extension `build/test-results/**/*.xml'`.

###### 2.3.4 Archive Stage ######
This stage will archive in Jenkins the archive files generated during the Assemble Stage. We use the **archiveArtifacts** step to do so:

    stage ('Archive') {
        steps {
            echo 'Archiving...'
            dir('CA2/Part1/gradle_basic_demo') {
                archiveArtifacts 'build/distributions/*'
            }
        }
    }

After a successful build, these artifacts (generally the zip, jar or war files) can be downloaded again.

##### 2.4 Provide the Jenkinsfile to the pipline Job #####
Now that the Jenkinsfile has been configured, we then push the changes to the repository and go back to the Job's configuration page.

On the Pipeline tab, change the definition to *Pipeline script from SCM* and confirm that the SCM is *Git*.

Then specify the repository URL (*https://bitbucket.org/angelmagalhaes_1211750/devops-21-22-lmn-1211750*) and select the already configured credentials (*ASM Bitbucket Credentials*). Then finally, specify the relative path for the Jenkinsfile (relative from the repository). In this case it should be in *CA2/Part1/gradle_basic_demo/Jenkinsfile*. Save the configuration changes to then be redirected to the job's page.

#### 3. Running the Build ####
On the Job's page, we can then run the build by pressing **Build Now** in the Dashboard. The build will execute all the declared stages in the Jenkinsfile.

If all goes well, we should see the output result for all the stages in the stage view, including one for the Checkout SCM for the Declarative Pipeline script:

![Stage View Result](./1-stage-view.JPG)

We can then view with more detail for the build by clicking on the build number:

![Build Result](./2-build-view.JPG)

You can see relevant information for the build, including the archived Build Artifacts and the Test Results. You can also check the Console Output in the dashboard to check how the script was executed. Checking the Console Log can be a good practice to check what went wrong in case of a failed build.