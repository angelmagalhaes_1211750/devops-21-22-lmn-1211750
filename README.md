# README #

Tutorial React.js and Spring Data REST
Tutorial available at
https://spring.io/guides/tutorials/react-and-spring-data-rest/
Source code available at
https://github.com/spring-guides/tut-react-and-spring-data-rest

This tutorial shows a collection of apps that use Spring Data REST and its powerful backend functionality, 
combined with React’s sophisticated features to build an easy-to-understand UI.

Spring Data REST provides a fast way to build hypermedia-powered repositories.
React is Facebook’s solution to efficient, fast, and easy-to-use views in JavaScript.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
